#!/bin/bash
set -euo pipefail

DIR=$(dirname "$(readlink -f "$0")")

systemd_dir="/usr/lib/systemd/system"
units_dir=$(realpath "$1")

[ ! -d "$units_dir" ] && echo "\"${units_dir}\" not found" && exit 1

unit_files=$(find "$units_dir" -mindepth 1 -name '*.service' -print -o -name '*.timer' -print)
dead_links=$(find "$systemd_dir" -type l ! -exec test -e {} \; -print)

echo installing systemd-status-mail helper
apt-get install -y aha
cp -v "${DIR}/systemd-status-mail" /usr/local/bin/systemd-status-mail

while IFS= read -r file; do
  if [[ -z $file ]]; then
    continue
  fi

  target=$(readlink -n "$file")
  source_dir=$(realpath $(dirname "$target"))

  if [[ $source_dir == "$units_dir" ]]; then
    echo "removing orphaned unit link ${file}"
    rm -v "$file"
  else
    echo "ignoring orphaned unit link ${file}. Not pointing to file inside ${units_dir}"
  fi

  echo ""
done <<<"$dead_links"

while IFS= read -r file; do
  filename="${file##*/}"
  systemd_file="${systemd_dir}/${filename}"

  echo "$filename"

  if [[ -f $systemd_file && ! -L $systemd_file ]]; then
    echo "existing $systemd_file is not a symlink. Will not overwrite"
    exit 1
  fi

  rm -rf "$systemd_file"
  ln -s "$file" "$systemd_file"

  state=$(systemctl is-enabled "$filename" || true)
  echo -e "state: $state\n"
done <<<"$unit_files"

echo -e "reloading systemd daemon\n"
systemctl daemon-reload

echo "done. You must manually enable services/timers"
