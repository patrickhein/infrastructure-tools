#!/bin/bash
set -euo pipefail

base_directory="$1"

for directory in "$base_directory"/*/; do
  echo ""

  compose=$(basename "$directory")
  echo deploying "$compose"

  environment_file="${directory}/environment.env"
  environment_file_argument=""

  if [[ -f "$environment_file" ]]; then
    echo "loading environment file"
    environment_file_argument="--env-file $environment_file"
  fi

  docker-compose $environment_file_argument --project-directory "$directory" pull || true
  docker-compose $environment_file_argument --project-directory "$directory" up --remove-orphans -d
done
